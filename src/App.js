import { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import ButtonComponent from './components/ButtonComponent';
import HeaderComponent from './components/HeaderComponent';



function App() {
  const[value, setValue]=useState("xin chào")
  const[number, setNumber]=useState(0)
  const handleCount=()=>{
    setNumber(number+1);
  }
  const Subtraction=()=>{
    if(number>0){
      setNumber(number-1);
    }
  }

  
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>{number}</div> 
        <div onClick={() => setValue("Lại xin chào")}>{value}</div>
        <button className='btn btn-success' onClick={handleCount}>Tăng giá trị</button>
        <ButtonComponent classButton="btn btn-danger">delete</ButtonComponent>
        <ButtonComponent classButton="btn btn-success" clickButton={Subtraction}>Giảm giá trị</ButtonComponent>
        <ButtonComponent classButton="btn btn-warning" value="warning"/>
        <HeaderComponent></HeaderComponent>
     
      </header>
    </div>
  );
}
export default App;
