const ButtonComponent=({...props})=>{
    return props.value 
    ?
    <button className={props.classButton} onclick={props.clickButton}>{props.value}</button>:
    <button className={props.classButton} onClick={props.clickButton}>{props.children}</button>  
}
export default ButtonComponent;
